# For merge request author(s)

Please go through the following merge request checklist and verify each of its steps as you complete them, replacing the `[ ]` check markers with an `[x]` (or by clicking on them):

- [ ] assign the merge request to yourself
- [ ] self-review the merge requests code, making sure it fixes its GitLab issue and is up to our coding standards
- [ ] write **at least some** decent tests, that are representative of its GitLab issue
- [ ] try to improve our test coverage, **do not decrease it**
- [ ] update the merge request git branch, if it's out-of-date with its base branch
- [ ] check the merge request for potential git merge conflicts, and resolve them if found
- [ ] make sure that all of the GitLab CI pipeline jobs are passing
- [ ] request at least one of your peers as reviewers
- [ ] finally, mark the merge request as ready for peer-review

If you deem any of the steps above not to be applicable to this merge request, do not remove them! Please provide a short explanation instead and then delete their `[ ]` check markers.

# For merge request reviewer(s)

Please go through the following merge request checklist and verify each of its steps as you complete them, replacing the `[ ]` check markers with an `[x]` (or by clicking on them):

- [ ] update the merge request git branch, if it's out-of-date with its base branch
- [ ] verify that the merge request has no git merge conflicts against its base branch
- [ ] verify that all of the GitLab CI pipeline jobs are passing
- [ ] review the merge requests code, making sure it fixes its GitLab issue and is up to our coding standards
- [ ] verify there are **at least some** decent tests, that are representative of its GitLab issue
- [ ] finally, merge the merge request

If you deem any of the steps above not to be applicable to this merge request, do not remove them! Please provide a short explanation instead and then delete their `[ ]` check markers.

---
