import tempfile
from unittest import TestCase

import locust_csv2openmetrics as lom


class EdgeCasesTests(TestCase):
    def test_normalize_failures_ratio_when_no_requests_have_been_made(self) -> None:
        response = lom.normalize_failures_ratio(
            locust_stats={
                "Failure Count": "0",
                "Request Count": "0",
            },
        )
        self.assertEqual(response, 0.0)

    def test_normalize_response_time_when_no_requests_have_been_made(self) -> None:
        response = lom.normalize_response_time("N/A")
        self.assertEqual(response, 0.0)

    def test_parse_locust_csv_stats_file_when_empty(self) -> None:
        with (
            tempfile.NamedTemporaryFile() as fp,
            self.assertRaisesRegex(
                ValueError,
                f"No 'Aggregated' locust results found in {fp.name}",
            ),
        ):
            lom.parse_locust_csv_stats_file(fp.name)
