import shutil
import tempfile
from textwrap import dedent
from unittest import TestCase, mock

import locust_csv2openmetrics as lom


class ExampleLocustStatsCSVTests(TestCase):
    maxDiff = None

    def setUp(self) -> None:
        self.csv_file = tempfile.NamedTemporaryFile()
        shutil.copyfile("examples/locust_stats.csv", self.csv_file.name)

        self.openmetrics_file = tempfile.NamedTemporaryFile("w+t")

    def check(
        self,
        cli_args_fmt: str,
        expected_output: str,
    ) -> None:
        cli_args = cli_args_fmt.format(
            csv_file=self.csv_file.name,
            openmetrics_file=self.openmetrics_file.name,
        )
        lom.main(cli_args.split())
        self.assertEqual(self.openmetrics_file.read(), expected_output)

    def check_with_default_openmetrics_file(
        self,
        cli_args_fmt: str,
        expected_output: str,
    ) -> None:
        with mock.patch(
            target="locust_csv2openmetrics.DEFAULT_OPENMETRICS_FILE",
            new=self.openmetrics_file.name,
        ):
            self.check(cli_args_fmt, expected_output)

    def test_with_no_labels(self) -> None:
        self.check_with_default_openmetrics_file(
            cli_args_fmt="{csv_file}",
            expected_output=dedent(
                """\
                # HELP locust_failures_ratio ratio of failed to total requests
                # TYPE locust_failures_ratio gauge
                # UNIT locust_failures_ratio ratio
                locust_failures_ratio 8.172872601261892e-06
                # HELP locust_requests_per_second average number of requests per second
                # TYPE locust_requests_per_second gauge
                locust_requests_per_second 67.98708320102457
                # HELP locust_response_time_p50_seconds 50% (aka median) response time
                # TYPE locust_response_time_p50_seconds gauge
                # UNIT locust_response_time_p50_seconds seconds
                locust_response_time_p50_seconds 0.54
                # HELP locust_response_time_p95_seconds 95% response time
                # TYPE locust_response_time_p95_seconds gauge
                # UNIT locust_response_time_p95_seconds seconds
                locust_response_time_p95_seconds 1.9
                # EOF
                """,
            ),
        )

    def test_with_empty_labels(self) -> None:
        self.check_with_default_openmetrics_file(
            cli_args_fmt="{csv_file} --labels",
            expected_output=dedent(
                """\
                # HELP locust_failures_ratio ratio of failed to total requests
                # TYPE locust_failures_ratio gauge
                # UNIT locust_failures_ratio ratio
                locust_failures_ratio 8.172872601261892e-06
                # HELP locust_requests_per_second average number of requests per second
                # TYPE locust_requests_per_second gauge
                locust_requests_per_second 67.98708320102457
                # HELP locust_response_time_p50_seconds 50% (aka median) response time
                # TYPE locust_response_time_p50_seconds gauge
                # UNIT locust_response_time_p50_seconds seconds
                locust_response_time_p50_seconds 0.54
                # HELP locust_response_time_p95_seconds 95% response time
                # TYPE locust_response_time_p95_seconds gauge
                # UNIT locust_response_time_p95_seconds seconds
                locust_response_time_p95_seconds 1.9
                # EOF
                """,
            ),
        )

    def test_with_one_label(self) -> None:
        self.check_with_default_openmetrics_file(
            cli_args_fmt="{csv_file} --labels product=xyz",
            expected_output=dedent(
                """\
                # HELP locust_failures_ratio ratio of failed to total requests
                # TYPE locust_failures_ratio gauge
                # UNIT locust_failures_ratio ratio
                locust_failures_ratio{product="xyz"} 8.172872601261892e-06
                # HELP locust_requests_per_second average number of requests per second
                # TYPE locust_requests_per_second gauge
                locust_requests_per_second{product="xyz"} 67.98708320102457
                # HELP locust_response_time_p50_seconds 50% (aka median) response time
                # TYPE locust_response_time_p50_seconds gauge
                # UNIT locust_response_time_p50_seconds seconds
                locust_response_time_p50_seconds{product="xyz"} 0.54
                # HELP locust_response_time_p95_seconds 95% response time
                # TYPE locust_response_time_p95_seconds gauge
                # UNIT locust_response_time_p95_seconds seconds
                locust_response_time_p95_seconds{product="xyz"} 1.9
                # EOF
                """,
            ),
        )

    def test_with_multiple_labels(self) -> None:
        self.check_with_default_openmetrics_file(
            cli_args_fmt="{csv_file} --labels product=xyz environment=staging test_scenario=simulate_production",
            expected_output=dedent(
                """\
                # HELP locust_failures_ratio ratio of failed to total requests
                # TYPE locust_failures_ratio gauge
                # UNIT locust_failures_ratio ratio
                locust_failures_ratio{environment="staging",product="xyz",test_scenario="simulate_production"} 8.172872601261892e-06
                # HELP locust_requests_per_second average number of requests per second
                # TYPE locust_requests_per_second gauge
                locust_requests_per_second{environment="staging",product="xyz",test_scenario="simulate_production"} 67.98708320102457
                # HELP locust_response_time_p50_seconds 50% (aka median) response time
                # TYPE locust_response_time_p50_seconds gauge
                # UNIT locust_response_time_p50_seconds seconds
                locust_response_time_p50_seconds{environment="staging",product="xyz",test_scenario="simulate_production"} 0.54
                # HELP locust_response_time_p95_seconds 95% response time
                # TYPE locust_response_time_p95_seconds gauge
                # UNIT locust_response_time_p95_seconds seconds
                locust_response_time_p95_seconds{environment="staging",product="xyz",test_scenario="simulate_production"} 1.9
                # EOF
                """,
            ),
        )

    def test_with_timestamp(self) -> None:
        self.check_with_default_openmetrics_file(
            cli_args_fmt="{csv_file} --timestamp 1695116107",
            expected_output=dedent(
                """\
                # HELP locust_failures_ratio ratio of failed to total requests
                # TYPE locust_failures_ratio gauge
                # UNIT locust_failures_ratio ratio
                locust_failures_ratio 8.172872601261892e-06 1695116107
                # HELP locust_requests_per_second average number of requests per second
                # TYPE locust_requests_per_second gauge
                locust_requests_per_second 67.98708320102457 1695116107
                # HELP locust_response_time_p50_seconds 50% (aka median) response time
                # TYPE locust_response_time_p50_seconds gauge
                # UNIT locust_response_time_p50_seconds seconds
                locust_response_time_p50_seconds 0.54 1695116107
                # HELP locust_response_time_p95_seconds 95% response time
                # TYPE locust_response_time_p95_seconds gauge
                # UNIT locust_response_time_p95_seconds seconds
                locust_response_time_p95_seconds 1.9 1695116107
                # EOF
                """,
            ),
        )

    def test_with_customized_openmetrics_file(self) -> None:
        self.check(
            cli_args_fmt="{csv_file} --openmetrics-file {openmetrics_file}",
            expected_output=dedent(
                """\
                # HELP locust_failures_ratio ratio of failed to total requests
                # TYPE locust_failures_ratio gauge
                # UNIT locust_failures_ratio ratio
                locust_failures_ratio 8.172872601261892e-06
                # HELP locust_requests_per_second average number of requests per second
                # TYPE locust_requests_per_second gauge
                locust_requests_per_second 67.98708320102457
                # HELP locust_response_time_p50_seconds 50% (aka median) response time
                # TYPE locust_response_time_p50_seconds gauge
                # UNIT locust_response_time_p50_seconds seconds
                locust_response_time_p50_seconds 0.54
                # HELP locust_response_time_p95_seconds 95% response time
                # TYPE locust_response_time_p95_seconds gauge
                # UNIT locust_response_time_p95_seconds seconds
                locust_response_time_p95_seconds 1.9
                # EOF
                """,
            ),
        )

    def test_with_all_cli_args(self) -> None:
        self.check(
            cli_args_fmt="{csv_file} --openmetrics-file {openmetrics_file} --timestamp 1695134542 --labels product=xyz environment=staging test_scenario=simulate_production",
            expected_output=dedent(
                """\
                # HELP locust_failures_ratio ratio of failed to total requests
                # TYPE locust_failures_ratio gauge
                # UNIT locust_failures_ratio ratio
                locust_failures_ratio{environment="staging",product="xyz",test_scenario="simulate_production"} 8.172872601261892e-06 1695134542
                # HELP locust_requests_per_second average number of requests per second
                # TYPE locust_requests_per_second gauge
                locust_requests_per_second{environment="staging",product="xyz",test_scenario="simulate_production"} 67.98708320102457 1695134542
                # HELP locust_response_time_p50_seconds 50% (aka median) response time
                # TYPE locust_response_time_p50_seconds gauge
                # UNIT locust_response_time_p50_seconds seconds
                locust_response_time_p50_seconds{environment="staging",product="xyz",test_scenario="simulate_production"} 0.54 1695134542
                # HELP locust_response_time_p95_seconds 95% response time
                # TYPE locust_response_time_p95_seconds gauge
                # UNIT locust_response_time_p95_seconds seconds
                locust_response_time_p95_seconds{environment="staging",product="xyz",test_scenario="simulate_production"} 1.9 1695134542
                # EOF
                """,
            ),
        )
