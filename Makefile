DOCKER_WORKDIR = /app
DOCKER_RUN = docker run --rm --user="$(shell id -u):$(shell id -g)" --volume="$(PWD)":$(DOCKER_WORKDIR)

# Verbosity settings
VERBOSITY_UNIT_TESTS = --verbose

# When running tests, `quiet=1` can be passed to the `make` command, making test output less verbose and disabling logs.
ifdef quiet
	VERBOSITY_UNIT_TESTS = --quiet
endif

# If the 'test.one' target is invoked, convert the rest of make CLI arguments to unit test label(s)
ifeq ($(firstword $(MAKECMDGOALS)),$(filter $(firstword $(MAKECMDGOALS)),test.one))
  TEST_LABELS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(TEST_LABELS):;@:)
endif



help: ## Display this help message
	@echo "Usage: make \033[32m[quiet=1] [PYTHONWARNINGS=all]\033[0m \033[36m[TARGET]\033[0m ...\n\nTargets:"
	@awk -F ':|##' '/^[^\t].+?:.*?##/ { \
		printf "  \033[36m%-36s\033[0m %s\n", $$1, $$NF \
	}' $(MAKEFILE_LIST)



coverage.report:
	@echo
	@coverage report

coverage.xml:
	@coverage xml

coverage.html:
	@coverage html

coverage: coverage.report coverage.xml coverage.html



test.unit: ## Run the unit tests
	coverage run -m unittest $(VERBOSITY_UNIT_TESTS)

test.one: ## Run only a specific unit test (module, module.TestCase or module.TestCase.test_method)
	coverage run -m unittest $(VERBOSITY_UNIT_TESTS) -k $(TEST_LABELS)

test: test.unit coverage ## Run the entire test suite (unit tests, coverage, ...)



code-quality.formatting: ## Check code formatting
	black --check --diff .
	isort --check --diff .

code-quality.lint: ## Check for syntactical and stylistic problems
	mypy .
	ruff check --show-source .
	doc8 .

code-quality.lock-file: ## Check for inconsistent lock file(s)
	poetry check --lock

code-quality.insecure-code: ## Check for insecure code
	bandit --recursive --severity-level=all .

code-quality.insecure-dependencies: ## Check for insecure dependencies
# There were multiple tool candidates here, but most of them had serious issues:
# - they required a commercial/paid license (i.e. 'pyup', 'snyk')
# - they were non-python based, requiring either an OS install or to be dockerized (i.e. 'trivy')
#
# However there are a few interesting Open Source and python based options:
# 1. https://github.com/pypa/pip-audit
# 2. https://github.com/twu/skjold
#
# 'pip-audit' is a project hosted by the Python Packaging Authority, which would
# be making it a preferred community choice. While I (@petar.maric) don't doubt
# it will become quite feature-rich in the future, currently (2023-10-03) it
# doesn't work with Poetry and supports only two security advisory databases:
# 1. PyPA Advisory Database, https://github.com/pypa/advisory-db
# 2. OSV.dev Database, https://osv.dev
#
# 'skjold' seems to be less popular at the moment and with fewer contributors,
# but does work with Poetry and supports five security advisory databases:
# 1. GitHub Advisory Database, https://github.com/advisories
# 2. PyUP.io safety-db, https://github.com/pyupio/safety-db
# 3. GitLab gemnasium-db, https://gitlab.com/gitlab-org/security-products/gemnasium-db
# 4. PyPA Advisory Database, https://github.com/pypa/advisory-db
# 5. OSV.dev Database, https://osv.dev
	skjold audit poetry.lock

GITLEAKS_RUN = $(DOCKER_RUN) zricethezav/gitleaks --no-banner --verbose --source=$(DOCKER_WORKDIR)
code-quality.exposed-secrets: ## Check for exposed hardcoded secrets
	$(GITLEAKS_RUN) detect
	$(GITLEAKS_RUN) protect

code-quality: ## Run all code quality checks
code-quality: code-quality.formatting
code-quality: code-quality.lint
code-quality: code-quality.lock-file
code-quality: code-quality.insecure-code
code-quality: code-quality.insecure-dependencies
code-quality: code-quality.exposed-secrets
