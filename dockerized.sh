#!/bin/bash -e

if [[ "${DOCKERIZED_DEBUG}" = "" ]]; then
    DOCKER_BUILD_EXTRA_OPTIONS='--quiet'
else
    DOCKER_BUILD_EXTRA_OPTIONS='--pull --no-cache'
fi


docker build --file Dockerfile.dev --tag local/locust_csv2openmetrics ${DOCKER_BUILD_EXTRA_OPTIONS} .
docker run --rm -it --user="$(id -u):$(id -g)" --volume="$PWD":/app local/locust_csv2openmetrics $*
